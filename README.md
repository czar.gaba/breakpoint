# breakpoint

css media query breakpoint

```
320px — 480px: Mobile devices
481px — 768px: iPads, Tablets
769px — 1024px: Small screens
1025px — 1200px: Large ipad
1201px - Mini Desktop
1366 - 1440px -  Small Desktop
1600px - 1680 - Medium Desktop 
1920 - Large Desktop 
```

css/scss code:
```
/* Global */

/* End Global */


/* Media Query */
@media only screen and (max-width:1920px){}
@media only screen and (max-width:1680px){}
@media only screen and (max-width:1440px){}
@media only screen and (max-width:1200px){}
@media only screen and (max-width:1024px){}
@media only screen and (max-width:768px){}
@media only screen and (max-width:480px){}

```
